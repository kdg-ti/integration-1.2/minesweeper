package be.kdg.minesweeper.view;

import be.kdg.minesweeper.model.Cell;
import be.kdg.minesweeper.model.Prediction;
import javafx.scene.image.Image;

public enum Tile {
    EMPTY_0("images/empty_0.png"),
    EMPTY_1("images/empty_1.png"),
    EMPTY_2("images/empty_2.png"),
    EMPTY_3("images/empty_3.png"),
    EMPTY_4("images/empty_4.png"),
    EMPTY_5("images/empty_5.png"),
    EMPTY_6("images/empty_6.png"),
    EMPTY_7("images/empty_7.png"),
    EMPTY_8("images/empty_8.png"),
    UNKNOWN("images/unknown.png"),
    FLAG("images/flag.png"),
    MAYBE("images/maybe.png"),
    BOMB("images/bomb.png");


    private final Image image;

    Tile(String url) {
        this.image = new Image(url);
    }

    public Image getImage() {
        return this.image;
    }

    public static Tile get(Cell cell, int neighbouringBombs) {
        if (!cell.isRevealed()) {
            return switch (cell.getPrediction()) {
                case FLAG ->  FLAG;
                case MAYBE -> MAYBE;
                default -> UNKNOWN;
            };
        } else if (cell.hasBomb()) {
            return BOMB;
        } else {
            // ordinals 0-8 are the same as that number of empty fields
            return values()[neighbouringBombs];
        }
    }
}
