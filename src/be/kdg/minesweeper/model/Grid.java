package be.kdg.minesweeper.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Grid {
    private final Cell[][] cells;
    private final Difficulty difficulty;
    private int numberOfRevealedCells;

    Grid(Difficulty difficulty) {
        this.difficulty = difficulty;
        this.cells = new Cell[difficulty.getWidth()][difficulty.getHeight()];
        this.numberOfRevealedCells = 0;
        this.initialise();
    }

    Difficulty getDifficulty() {
        return difficulty;
    }

    private void initialise() {
        int row;
        int column;
        for (row = 0; row < getRowCount(); row++) {
            for (column = 0; column < getColumnCount(); column++) {
                this.cells[row][column] = new Cell(row, column);
            }
        }

        final Random random = new Random();
        int bombsPlaced = 0;
        while (bombsPlaced < this.difficulty.getBombs()) {
            row = random.nextInt(getRowCount());
            column = random.nextInt(getColumnCount());
            if (!this.cells[row][column].hasBomb()) {
                this.cells[row][column].setHasBomb();
                bombsPlaced++;
            }
        }
    }

    private int getRowIndexAbove(int row) {
        if (row == 0) {
            return row;
        } else {
            return row - 1;
        }
    }

    private int getRowIndexBelow(int row) {
        if (row == this.cells.length - 1) {
            return row;
        } else {
            return row + 1;
        }
    }

    private int getColumnIndexLeftOf(int column) {
        if (column == 0) {
            return column;
        } else {
            return column - 1;
        }
    }

    private int getColumnIndexRightOf(int column) {
        if (column == this.cells[0].length - 1) {
            return column;
        } else {
            return column + 1;
        }
    }

    List<Cell> getAllBombs() {
        final List<Cell> returnList = new ArrayList<>();

        for (Cell[] row : cells) {
            for (Cell cell : row) {
                if (cell.hasBomb()) {
                    cell.setRevealed();
                    returnList.add(cell);
                }
            }
        }

        return returnList;
    }

    private void getCellsToReveal(int row, int column, List<Cell> revealedCells) {
        if (row >= 0 && row < this.cells.length && column >= 0 && column < this.cells[0].length) {
            Cell centerCell = cells[row][column];
            if (!centerCell.hasBomb() && !centerCell.isRevealed()) {
                centerCell.setRevealed();
                numberOfRevealedCells++;
                revealedCells.add(centerCell);
                if (getNeighbouringBombs(centerCell.getRow(), centerCell.getColumn()) == 0) {
                    getCellsToReveal(row - 1, column - 1, revealedCells);
                    getCellsToReveal(row - 1, column, revealedCells);
                    getCellsToReveal(row - 1, column + 1, revealedCells);
                    getCellsToReveal(row, column - 1, revealedCells);
                    getCellsToReveal(row, column + 1, revealedCells);
                    getCellsToReveal(row + 1, column - 1, revealedCells);
                    getCellsToReveal(row + 1, column, revealedCells);
                    getCellsToReveal(row + 1, column + 1, revealedCells);
                }
            }
        }
    }

    List<Cell> getCellsToReveal(int row, int column) {
        final List<Cell> revealedCells = new ArrayList<>();
        getCellsToReveal(row, column, revealedCells);
        return revealedCells;
    }

    int getNeighbouringBombs(int row, int column) {
        if (!this.cells[row][column].hasBomb()) {
            int amountOfNeighbouringBombs = 0;

            for (int otherRow = getRowIndexAbove(row); otherRow <= getRowIndexBelow(row); otherRow++) {
                for (int otherColumn = getColumnIndexLeftOf(column); otherColumn <= getColumnIndexRightOf(column); otherColumn++) {
                    if (this.cells[otherRow][otherColumn].hasBomb()) {
                        amountOfNeighbouringBombs++;
                    }
                }
            }

            return amountOfNeighbouringBombs;
        } else {
            return 0;
        }
    }

    Cell getCell(int i, int j) {
        return this.cells[i][j];
    }

    int getRowCount() {
        return this.cells.length;
    }

    int getColumnCount() {
        return this.cells[0].length;
    }

    int getNumberOfRevealedCells() {
        return this.numberOfRevealedCells;
    }
}
